#include<stdio.h>

#include<freetype2/ft2build.h>
#include FT_FREETYPE_H

int main()
{

    FT_Library ft;
    FT_Error err = FT_Init_FreeType(&ft);
    if (err != 0)
    {
        printf("Failed to initialize FreeType\n");
        return 1;
    }
    FT_Int major, minor, patch;
    FT_Library_Version(ft, &major, &minor, &patch);
    printf("Freetype's version is: %d.%d.%d\n", major, minor, patch);

    char *ubuntu_mono = "./fonts/UbuntuMono-B.ttf";

    char *name = "ishanjain";
    int name_len = sizeof(name);
    FT_Face *glyphs = malloc(sizeof(FT_Face) * name_len);

    for(int j = 0; j < name_len; j++)
    {
        err = FT_New_Face(ft, ubuntu_mono, 0, &glyphs[j]);
        if (err != 0)
        {
            printf("Failure to load font face\n");
            return 1;
        }
        err = FT_Set_Pixel_Sizes(glyphs[j], 0, 32);
        if (err != 0)
        {
            printf("Failed to set pixel size\n");
            return 1;
        }

        FT_UInt glyph_index = FT_Get_Char_Index(glyphs[j], name[j]);
        FT_Int32 load_flags = FT_LOAD_DEFAULT;
        err = FT_Load_Glyph(glyphs[j], glyph_index, load_flags);
        if (err != 0)
        {
            printf("Failed to load glyph\n");
            return 1;
        }

        FT_Int32 render_flags = FT_RENDER_MODE_NORMAL;
        err = FT_Render_Glyph(glyphs[j]->glyph, render_flags);
        if (err != 0)
        {
            printf("Failed to render the glyph\n");
            return 1;
        }

         printf("Face width=%d, rows=%d, pitch=%d\n",
                 glyphs[j]->glyph->bitmap.width,
                 glyphs[j]->glyph->bitmap.rows,
                 glyphs[j]->glyph->bitmap.pitch);
    }
    for (size_t i = 0; i < glyphs[0]->glyph->bitmap.rows; i++)
    {
        for (int j = 0; j < name_len; j++)
        {
            for (size_t k = 0; k < glyphs[j]->glyph->bitmap.width; k++)
            {
                unsigned char pixel_brightness =
                    glyphs[j]->glyph->bitmap.buffer[i * glyphs[j]->glyph->bitmap.pitch + k];

                if (pixel_brightness > 169)
                {
                    printf("*");
                }
                else if (pixel_brightness > 84)
                {
                    printf(".");
                }
                else {
                    printf(" ");
                }
           }
        }
        printf("\n");
    }
    return 0;
}
